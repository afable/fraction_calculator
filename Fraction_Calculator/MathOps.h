//
//  MathOps.h
//  FractionTest
//
//  Created by afable on 11-08-25.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Fraction.h"


@interface Fraction (MathOps)
-(Fraction *) add: (Fraction *)f;
-(Fraction *) sub: (Fraction *)f;
-(Fraction *) mul: (Fraction *)f;
-(Fraction *) div: (Fraction *)f;
-(Fraction *) invert;

@end
