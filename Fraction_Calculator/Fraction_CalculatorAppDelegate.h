//
//  Fraction_CalculatorAppDelegate.h
//  Fraction_Calculator
//
//  Created by afable on 11-09-04.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@class Fraction_CalculatorViewController;

@interface Fraction_CalculatorAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet Fraction_CalculatorViewController *viewController;

@end
