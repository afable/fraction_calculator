//
//  Fraction.h
//  FractionTest
//
//  Created by afable on 11-08-23.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Fraction : NSObject
{
@private
    int numerator;
    int denominator;
}

@property int numerator, denominator;

+(Fraction *) fractionWithNumerator: (int)a andDenominator: (int)b;
+(Fraction *) fraction;
+(Fraction *) allocF;
+(int) count;

-(Fraction *) initWith: (int) n: (int) d;

-(void) setNumerator: (int)a andDenominator: (int)b;
-(void) print: (BOOL)reduced;
-(void) print;

-(double) convertToNum;
-(NSString *) convertToString;

-(void) reduce;

-(void) reset;




/* using categories instead!
 -(Fraction *) add: (Fraction *)f;
 -(Fraction *) subtract: (Fraction *) f;
 -(Fraction *) multiply: (Fraction *) f; 
 -(Fraction *) divide: (Fraction *) f;
 */

-(int) compare: (Fraction *) f;

-(NSString *) description; // override description like overriding Java's toString()


@end