//
//  Comparison.h
//  FractionTest
//
//  Created by afable on 11-08-26.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Fraction.h"

@interface Fraction (Comparison)
-(BOOL) isEqualTo: (Fraction *) f;
-(NSComparisonResult) compare: (id) f;
//-(void) print; //Fraction's print method can be overriden
//-(Fraction *) invert; //Even though MathOps category has this, we can still define it here


@end
