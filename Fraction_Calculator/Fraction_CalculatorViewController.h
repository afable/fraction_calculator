//
//  Fraction_CalculatorViewController.h
//  Fraction_Calculator
//
//  Created by afable on 11-09-04.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Calculator.h"

@interface Fraction_CalculatorViewController : UIViewController {
    UILabel *display;
    char op;
    int currentNumber;
    NSMutableString *displayString;
    BOOL firstOperand, isNumerator;
    Calculator *myCalculator;
    BOOL convert;
    BOOL isNeg;
    BOOL isFirstOpNeg;
    BOOL isSecondOpNeg;
    BOOL clickedOver;
    BOOL digitFirst;
    int negPos;
}

@property (nonatomic, retain) IBOutlet UILabel *display;
@property (nonatomic, retain) NSMutableString *displayString;

-(void) processDigit: (int) digit;
-(void) processOp: (char) op;
-(void) storeFracPart;

//numeric keys
-(IBAction) clickDigit: (id) sender;

//arithmetic operation keys
-(IBAction) clickPlus: (id) sender;
-(IBAction) clickMinus: (id) sender;
-(IBAction) clickMultiply: (id) sender;
-(IBAction) clickDivide: (id) sender;

//unary operation keys
-(IBAction) clickNegative: (id) sender;

//misc. keys
-(IBAction) clickOver: (id) sender;
-(IBAction) clickEquals: (id) sender;
-(IBAction) clickClear: (id) sender;
-(IBAction) clickConvert: (id) sender;

@end
