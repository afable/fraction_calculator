//
//  Fraction_CalculatorViewController.m
//  Fraction_Calculator
//
//  Created by afable on 11-09-04.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Fraction_CalculatorViewController.h"

@implementation Fraction_CalculatorViewController

@synthesize displayString, display;

-(void) viewDidLoad
{
    //override point for customization after application launch
    
    firstOperand = YES;
    isNumerator = YES;
    self.displayString = [NSMutableString stringWithCapacity: 40];
    myCalculator = [[Calculator alloc] init];
}

-(void) processDigit:(int)digit
{
    currentNumber = currentNumber * 10 + digit;
    
    [displayString appendString: [NSString stringWithFormat: @"%i", digit]];
    display.text = displayString;
}

-(IBAction) clickDigit:(id)sender
{
    digitFirst = YES;
    
    int digit = [sender tag];
    
    [self processDigit: digit];
}

-(void) processOp:(char)theOp
{
    if (!digitFirst)
    {
        ; //numbers should be pressed first
    }
    else if (op) //if op has already been pressed, then we're doing a chain calculation. so make current binary expression into one term
    {
        @try
        {
            if (!isNumerator && !currentNumber) //if it's not the numerator and 0 is clicked
                @throw [NSException exceptionWithName: @"divisionByZero" reason:@"Division by zero!" userInfo:nil];
            else
            {
                [self storeFracPart]; //assuming at this point it will be operand2
                
                //handle negative calculation here!
                if (isFirstOpNeg)
                    [[myCalculator operand1] setNumerator: -([[myCalculator operand1] numerator])];            
                if (isSecondOpNeg)
                    [[myCalculator operand2] setNumerator: -([[myCalculator operand2] numerator])];            
                
                [myCalculator performOperation: op]; //perform operation with current op and set accumulator
                
                //have to handle negative display here because Fraction's reduce: method puts the negative on the denominator. basically, turn numerator negative and denominator positive if accumulator is negative
                if (myCalculator.accumulator.numerator < 0 || myCalculator.accumulator.denominator < 0)
                {
                    if (myCalculator.accumulator.numerator > 0)
                        myCalculator.accumulator.numerator = -(myCalculator.accumulator.numerator);
                    if (myCalculator.accumulator.denominator < 0)
                        myCalculator.accumulator.denominator = -(myCalculator.accumulator.denominator);
                }
                
                //set operand1 = accumulator and reset operand2
                [myCalculator.operand1 
                 setNumerator: myCalculator.accumulator.numerator 
                 andDenominator: myCalculator.accumulator.denominator];
                [myCalculator.operand2 reset];
                
                //display the pressed operator
                NSString *opStr;
                
                switch (theOp)
                {
                    case ('+'):
                        opStr = @" + ";
                        break;
                    case ('-'):
                        opStr = @" - ";
                        break;
                    case ('*'):
                        opStr = @" * ";
                        break;
                    case ('/'):
                        opStr = @" / ";
                        break;
                }
                
                //reset values for next operand (should be > 2)
                firstOperand = NO;
                isNumerator = YES;
                op = theOp;
                [displayString appendString: opStr];
                [display setText: displayString];
                currentNumber = 0;
                
                //reset isNeg and clickOver for new operand
                isNeg = NO;
                clickedOver = NO;
                isFirstOpNeg = NO;
                isSecondOpNeg = NO;
            }
        }
        @catch (NSException *exception) 
        {
            if ( [[exception name] caseInsensitiveCompare: @"divisionByZero"] == NSOrderedSame )
            {
                //          [displayString deleteCharactersInRange: NSMakeRange(0, [displayString length])];
                //          [displayString appendString: @"Error: division by zero"];
                
                //or could do it this way
                [displayString release];
                displayString = [[NSMutableString stringWithString: @"Error: division by zero"] retain]; //don't forget you need to retain, otherwise at some point the retainCount on displayString could go to zero and you'll have a pointer to a deallocated memory location
                [display setText: displayString];
                digitFirst = NO;
            }
            else
                NSLog (@"%@", [exception name]);
        }
    }
    else
    {
        NSString *opStr;
        
        switch (theOp)
        {
            case ('+'):
                opStr = @" + ";
                break;
            case ('-'):
                opStr = @" - ";
                break;
            case ('*'):
                opStr = @" * ";
                break;
            case ('/'):
                opStr = @" / ";
                break;
        }
        
        [self storeFracPart];
        firstOperand = NO;
        isNumerator = YES;
        
        op = theOp;
        
        [displayString appendString: opStr];
        [display setText: displayString];
        
        currentNumber = 0;
        isNumerator = YES;
        
        //reset isNeg and clickOver for new operand
        isNeg = NO;
        clickedOver = NO;
    }
}

-(void) storeFracPart //store frac should be called when the fraction is done
{
    if (firstOperand)
    {
        if (isNumerator)
        {
            myCalculator.operand1.numerator = currentNumber;
            myCalculator.operand1.denominator = 1; // e.g. 3 * 4/5 =
        }
        else
        {
            myCalculator.operand1.denominator = currentNumber;
        }
    }
    else if (isNumerator)
    {
        myCalculator.operand2.numerator = currentNumber;
        myCalculator.operand2.denominator = 1; // e.g. 3/2 * 4 =
    }
    else
    {
        myCalculator.operand2.denominator = currentNumber;
        firstOperand = YES;
    }
    
    currentNumber = 0; //reset current number after 'OVER' and before '='
}

//arithmetic operation keys
-(IBAction) clickPlus: (id) sender
{
    [self processOp: '+'];
}

-(IBAction) clickMinus: (id) sender
{
    [self processOp: '-'];
}

-(IBAction) clickMultiply: (id) sender
{
    [self processOp: '*'];
}

-(IBAction) clickDivide: (id) sender
{
    [self processOp: '/'];
}



//unary operations
-(IBAction) clickNegative: (id) sender
{
    if (!currentNumber && !clickedOver) //can only do if a digit hasn't been processed and clickedOver hasn't been pressed yet
    {   
        if (isNeg) //if NEG was already pressed, undo it
        {
            //flag which operand is negative
            if (firstOperand)
                isFirstOpNeg = NO;
            else
                isSecondOpNeg = NO;
            
            [displayString deleteCharactersInRange: NSMakeRange(negPos, 1)];
            [display setText: displayString];
            isNeg = NO;
        }
        else
        {
            //flag which operand is negative
            if (firstOperand)
                isFirstOpNeg = YES;
            else
                isSecondOpNeg = YES;
            
            negPos = [displayString length];
            [displayString appendString: @"-"];
            [display setText: displayString];
            isNeg = YES;
        }
    }
}


//misc. keys
-(IBAction) clickOver:(id)sender
{
    [self storeFracPart];
    isNumerator = NO;
    [displayString appendString: @"/"];
    [display setText: displayString];
    clickedOver = YES;
}

-(IBAction) clickEquals:(id)sender
{
    @try
    {
        if (!isNumerator && !currentNumber) //if it's not the numerator and 0 is clicked
            @throw [NSException exceptionWithName: @"divisionByZero" reason:@"Division by zero!" userInfo:nil];
        else
        {
            if (!op) //if operator hasn't been set yet, set it to '+'
                op = '+';

            [self storeFracPart];
            
            //handle negative calculation here!
            if (isFirstOpNeg)
                [[myCalculator operand1] setNumerator: -([[myCalculator operand1] numerator])];            
            if (isSecondOpNeg)
                [[myCalculator operand2] setNumerator: -([[myCalculator operand2] numerator])];            

            [myCalculator performOperation: op];
            [displayString appendString: @" = "];

            //have to handle negative display here because Fraction's reduce: method puts the negative on the denominator. basically, turn numerator negative and denominator positive if accumulator is negative
            if (myCalculator.accumulator.numerator < 0 || myCalculator.accumulator.denominator < 0)
            {
                if (myCalculator.accumulator.numerator > 0)
                    myCalculator.accumulator.numerator = -(myCalculator.accumulator.numerator);
                if (myCalculator.accumulator.denominator < 0)
                    myCalculator.accumulator.denominator = -(myCalculator.accumulator.denominator);
            }

            if (convert) // convert was pressed instead of equals
            {   
                [displayString appendFormat: @"%.2lf", [myCalculator.accumulator convertToNum]];
                [display setText: displayString];
            }
            else
            {
                [displayString appendString: [myCalculator.accumulator convertToString]];
                [display setText: displayString];
            }
        }
    }
    @catch (NSException *exception) 
    {
        if ( [[exception name] caseInsensitiveCompare: @"divisionByZero"] == NSOrderedSame )
        {
//          [displayString deleteCharactersInRange: NSMakeRange(0, [displayString length])];
//          [displayString appendString: @"Error: division by zero"];
            
            //or could do it this way
            [displayString release];
            displayString = [[NSMutableString stringWithString: @"Error: division by zero"] retain]; //don't forget you need to retain, otherwise at some point the retainCount on displayString could go to zero and you'll have a pointer to a deallocated memory location
            [display setText: displayString];
            digitFirst = NO;
        }
        else
            NSLog (@"%@", [exception name]);
    }
        
    
    //reset currentNum, isNumerator, firstOperand, displayString, convert, isNeg, isFirstOpNeg, isSecondOpNeg, clickedOver
    currentNumber = 0;
    isNumerator = YES;
    firstOperand = YES;
    convert = NO;
    isNeg = NO;
    isFirstOpNeg = NO;
    isSecondOpNeg = NO;
    clickedOver = NO;
    op = 0;
    [displayString setString: @""];
    
    //reset op1, op2, and accumulator
    [myCalculator.operand1 setNumerator: 0 andDenominator: 1];
    [myCalculator.operand2 setNumerator: 0 andDenominator: 1];
    [myCalculator clear];
    NSLog (@"------------");
}

-(IBAction) clickClear: (id) sender
{
    currentNumber = 0;
    isNumerator = YES;
    firstOperand = YES;
    convert = NO;
    isNeg = NO;
    isFirstOpNeg = NO;
    isSecondOpNeg = NO;
    clickedOver = NO;
    [myCalculator clear];
    
    [displayString setString: @""];
    [display setText: displayString];
}

-(IBAction) clickConvert: (id) sender
{
    convert = YES;
    [self clickEquals: sender];
}

- (void)dealloc
{
    [displayString release];
    [display release];
    [myCalculator release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
 // Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
 - (void)viewDidLoad
 {
 [super viewDidLoad];
 }
 */

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end


// fix calculator so that YOU MUST ENTER 2 operands and an operator to perform an operation!!!!