//
//  Calculator.m
//  Fraction_Calculator
//
//  Created by afable on 11-09-04.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Calculator.h"

@implementation Calculator

@synthesize operand1, operand2, accumulator;

-(id) init
{
    self = [super init];
    
    if (self)
    {
        operand1 = [[Fraction alloc] init];
        operand2 = [[Fraction alloc] init];
        accumulator = [[Fraction alloc] init];
    }
    
    return self;
}

-(void) dealloc
{
    [operand1 release]; 
    [operand2 release];
    [accumulator release];
    [super dealloc];
}

-(void) clear
{
    if (accumulator)
    {
        accumulator.numerator = 0;
        accumulator.denominator = 0;
    }
    
    if (operand1)
    {
        operand1.numerator = 0;
        operand1.denominator = 1;
    }
    
    if (operand2)
    {
        operand2.numerator = 0;
        operand2.denominator = 1;
    }
}

-(Fraction *) performOperation:(char)op
{
    Fraction *result;
    
    switch (op)
    {
        case ('+'):
            result = [operand1 add: operand2]; // remember, result is added to NSAutorelease pool!
            break;
        case ('-'):
            result = [operand1 sub: operand2];
            break;
        case ('*'):
            result = [operand1 mul: operand2];
            break;
        case ('/'):
            result = [operand1 div: operand2];
            break;
    }
    accumulator.numerator = result.numerator;
    accumulator.denominator = result.denominator;
    NSLog (@"Calculator:performOp: op1: %@, op2: %@, accumulator: %@", operand1, operand2, accumulator);
    if (!operand1.denominator || !operand2.denominator || !accumulator.denominator) // if any of these denominators are zero
        @throw [NSException exceptionWithName: @"divisionByZero" reason:@"Division by zero!" userInfo:nil];
    
    return accumulator;
}

@end
