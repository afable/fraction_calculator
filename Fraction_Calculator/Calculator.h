//
//  Calculator.h
//  Fraction_Calculator
//
//  Created by afable on 11-09-04.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Fraction.h" //xcode won't let me compile if I have the headers set in the build settings!!
#import "Comparison.h"
#import "MathOps.h"

@interface Calculator : NSObject
{
@private
    Fraction *operand1;
    Fraction *operand2;
    Fraction *accumulator;
}

@property (retain, nonatomic) Fraction *operand1, *operand2, *accumulator;

-(Fraction *) performOperation: (char) op;
-(void) clear;

@end
