//
//  Fraction.m
//  FractionTest
//
//  Created by afable on 11-08-23.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#import "Fraction.h"

static int gCounter;    // since gCounter is static, it is global to this source (.m) file but also local to this source (.m) file such that it can only be accessed from the methods in this file (Fraction.m)!

@implementation Fraction

@synthesize numerator, denominator;

+(Fraction *) fractionWithNumerator: (int)a andDenominator: (int)b
{
    Fraction *f = [[Fraction allocF] initWith: a : b];
    [f autorelease];
    return f;
}

+(Fraction *) fraction
{
    Fraction *f = [[Fraction allocF] initWith: 0 : 1];
    [f autorelease];
    return f;
}

- (id)init
{
    self = [super init];
    if (self) {
        numerator = 0;
        denominator = 1;
    }
    
    return self;
}

-(Fraction *) initWith: (int) n: (int) d
{
    self = [super init];
    
    if (self)
    {
        [self setNumerator: n andDenominator: d];
    }
    
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

-(void) setNumerator: (int)a andDenominator:(int)b
{
    numerator = a;
    denominator = b;
}

-(void) print: (BOOL) reduced
{
    int whole = 0, numTemp, denTemp;
    numTemp = numerator;
    denTemp = denominator;
    
    while (numTemp > denTemp)
    {
        numTemp -= denTemp;
        ++whole;
    }
    
    if (reduced)
    {
            Fraction * r = [[Fraction alloc] init];
            [r setNumerator: numerator andDenominator: denominator];
            [r reduce];
        
            int wholeR = 0, numTempR, denTempR;
            numTempR = r.numerator;
            denTempR = r.denominator;
            
            while (numTempR > denTempR)
            {
                numTempR -= denTempR;
                ++wholeR;
            }
        
            if (r.denominator == 1)
                printf("%i\n", r.numerator);
            else if (r.numerator <= r.denominator)
                printf("%i/%i\n", r.numerator, r.denominator);
            else
                if (numTempR % denTempR)
                    printf("%i %i/%i\n", wholeR, numTempR, r.denominator);
                else
                    printf("%i\n", ++wholeR);

                
            
            [r release];
    }
    else
    {
        if (numerator <= denominator)
            printf("%i/%i\n", numerator, denominator);
        else
            if (numTemp % denTemp)
                printf("%i %i/%i\n", whole, numTemp, denominator);
            else
                printf("%i\n", ++whole);

    }
}

-(void) print
{
    printf("%i/%i\n", numerator, denominator);
}

-(double) convertToNum
{
    if (denominator)
        return (double)numerator / denominator;
    else
        return 1.0;
}

-(NSString *) convertToString
{
    if (numerator == denominator)
        if (numerator == 0)
            return @"0"; //since denominator can't be zero!
        else
            return @"1"; 
    else if ( (denominator == 1) | (denominator == -1) )
        return [NSString stringWithFormat: @"%i", numerator];
    else
        return [NSString stringWithFormat: @"%i/%i", numerator, denominator];
}


-(void) reduce
{
    int u, v, temp;
    
    u = numerator;
    v = denominator;
    
    while ( v != 0 )
    {
        temp = u % v;
        u = v;
        v = temp;
    }
    
    numerator /= u;
    denominator /= u;
}


+(Fraction *) allocF
{
    //extern int gCounter; // good programming style
    ++gCounter;
    
    return [Fraction alloc];
}

+(int) count
{
    //extern int gCounter; // good programming style
    return gCounter;
}

//################################################################################
//#### Informal Protocol from super @interface NSObject (NSComparisonMethods) ####
//################################################################################
// maybe this takes less priority because it is a category of a super class? (the ROOT superclass NSObject)
- (BOOL)isEqualTo:(Fraction *)object // id pointer copying the location of the Fraction type.
{
    if (![object isMemberOfClass: [Fraction class]])
        return NO; // sender is not even of type [Fraction class], so obviously NO.
    
    Fraction *f1 = [[Fraction alloc] initWith: numerator : denominator];
    [f1 reduce];
    
    Fraction *f2 = [[Fraction alloc] initWith: [object numerator] : [object denominator]]; // have to use getters to retrieve numerator & denominator since id pointers cannot directly access an object's properties... The compiler needs to know the type to verify what properties a type has.
    [f2 reduce];
    
    BOOL isEqual;
    
    isEqual = (f1.numerator == f2.numerator) && (f1.denominator == f2.denominator);
    
    [f1 release];
    [f2 release];
    
    return isEqual;

}

// Implemented using isEqual:. Returns NO if receiver is nil.
- (BOOL)isLessThanOrEqualTo:(id)object;
{
    if (![object isMemberOfClass: [Fraction class]])
        return NO; // sender is not even of type [Fraction class], so obviously NO.
    
    Fraction *f1 = [[Fraction alloc] initWith: numerator : denominator];
    [f1 reduce];
    
    Fraction *f2 = [[Fraction alloc] initWith: [object numerator] : [object denominator]]; // have to use getters to retrieve numerator & denominator since id pointers cannot directly access an object's properties... The compiler needs to know the type to verify what properties a type has.
    [f2 reduce];
    
    BOOL isLessThanOrEqual;
    
    isLessThanOrEqual = ([f1 convertToNum] <= [f2 convertToNum]);

    
    [f1 release];
    [f2 release];
    
    return isLessThanOrEqual;
}

// Implemented using compare. Returns NO if receiver is nil.
- (BOOL)isLessThan:(id)object
{
    BOOL isLessThan;
    
    if ([self compare: object] < 0)
        isLessThan = 1;
    else
        isLessThan = 0;
    
    return isLessThan;
}

// Implemented using compare. Returns NO if receiver is nil.
- (BOOL)isGreaterThanOrEqualTo:(id)object
{
    BOOL isGreaterThanOrEqualTo;
    
    if ([self compare: object] >= 0)
        isGreaterThanOrEqualTo = 1;
    else
        isGreaterThanOrEqualTo = 0;
    
    return isGreaterThanOrEqualTo;
}

// Implemented using compare. Returns NO if receiver is nil.
- (BOOL)isGreaterThan:(id)object
{
    BOOL isGreaterThan;
    
    if ([self compare: object] > 0)
        isGreaterThan = 1;
    else
        isGreaterThan = 0;
    
    return isGreaterThan;
}

// Implemented using compare. Returns NO if receiver is nil.
- (BOOL)isNotEqualTo:(id)object
{
    BOOL isNotEqualTo;
    
    if ([self compare: object])
        isNotEqualTo = 1;
    else
        isNotEqualTo = 0;
    
    return isNotEqualTo;
}

-(int) compare:(Fraction *)f
{
    Fraction *f1 = [[Fraction alloc] initWith: numerator : denominator];
    [f1 reduce];
    
    Fraction *f2 = [[Fraction alloc] initWith: f.numerator : f.denominator];
    [f2 reduce];
    
    int compared;
    
    if ([f1 convertToNum] > [f2 convertToNum])
        compared = 1;
    else if ([f1 isEqualTo: f2])
        compared = 0;
    else
        compared = -1;
    
    [f1 release];
    [f2 release];
    
    return compared;
}

-(NSString *) description
{
    return [NSString stringWithFormat: @"%i/%i", numerator, denominator];
}

//subclasses of NSObject already conform to the NSCopying protocol, so just override the copyWithZone method!
-(id) copyWithZone: (NSZone *) zone
{
    //[self class] in case subclassed
    Fraction *newFract = [[[self class] allocWithZone: zone] initWith: numerator : denominator];
    return newFract;
}

//subclasses of NSObject already conform to the NSMutableCopying protocol, so just override the copyWithZone method!
-(id) mutableCopyWithZone: (NSZone *) zone
{
    //[self class] in case subclassed
    Fraction *newFract = [[[self class] allocWithZone: zone] initWith: numerator : denominator];
    return newFract;
}

-(void) reset
{
    numerator = 0;
    denominator = 1;
}

@end
