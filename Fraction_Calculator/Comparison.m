//
//  Comparison.m
//  FractionTest
//
//  Created by afable on 11-08-26.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Comparison.h"

@implementation Fraction (Comparison)
 -(BOOL) isEqualTo: (Fraction *) f
{
    Fraction *f1 = [[Fraction alloc] initWith: numerator : denominator];
    [f1 reduce];
    
    Fraction *f2 = [[Fraction alloc] initWith: [f numerator] : [f denominator]];
    [f2 reduce];
    
    BOOL isEqual;
    
    isEqual = (f1.numerator == f2.numerator) && (f1.denominator == f2.denominator);
    
    [f1 release];
    [f2 release];
    
    return isEqual;
}

-(NSComparisonResult) compare:(id)f
{
    if ( [self convertToNum] > [f convertToNum] )
        return NSOrderedDescending;
    else if ( [self convertToNum] == [f convertToNum] )
        return NSOrderedSame;
    else
        return NSOrderedAscending;
}

/* we can override Fraction's print: method
-(void) print
{
    printf("FUCK YOU!!!\n");
}
 */

/* we can define even though MathOps category contians this method
-(Fraction *) invert
{
    Fraction *f;
    return f;
}
 */

@end
