//
//  MathOps.m
//  FractionTest
//
//  Created by afable on 11-08-25.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MathOps.h"


@implementation Fraction (MathOps)
-(Fraction *) add: (Fraction *)f
{
    Fraction *result = [Fraction fraction];
    [result setNumerator: (numerator * f.denominator) + (f.numerator * denominator) 
          andDenominator: denominator * f.denominator];
//    [result autorelease]; //not needed because when init, automatically added to autorelease pool
    [result reduce];
    return result;
}

-(Fraction *) sub: (Fraction *)f
{
    Fraction *result = [Fraction fraction];
    [result setNumerator: (numerator * f.denominator) - (f.numerator * denominator)
          andDenominator: denominator * f.denominator];
//    [result autorelease]; //not needed because when init, automatically added to autorelease pool
    [result reduce];
    return result;
}

-(Fraction *) mul: (Fraction *)f
{
    if (!denominator || !f.denominator )
    {
        @throw [NSException exceptionWithName: @"divisionByZero" reason:@"Division by zero!" userInfo:nil];
    }
    else
    {
        Fraction *result = [Fraction fraction];
        [result setNumerator: numerator * f.numerator
              andDenominator: denominator * f.denominator];
//    [result autorelease]; //not needed because when init, automatically added to autorelease pool
        [result reduce];
        return result;
    }
}

-(Fraction *) div: (Fraction *)f
{
    if (!numerator || !denominator || !f.numerator || !f.denominator)
    {
        @throw [NSException exceptionWithName: @"divisionByZero" reason:@"Division by zero!" userInfo:nil];
        /* could have done this, since we're not allocating any memory and returning only a null reference... but better to end program
        id nothing;
        return nothing;
         */
    }
    else
    {
        Fraction *result = [Fraction fraction];
        [result setNumerator: numerator * f.denominator
              andDenominator: denominator * f.numerator];
//    [result autorelease]; //not needed because when init, automatically added to autorelease pool
        [result reduce];
        return result;
    }
}

-(Fraction *) invert
{
    Fraction *result = [Fraction fraction];
//    [result autorelease]; //not needed because when init, automatically added to autorelease pool
    [result reduce];
    return result;
}

@end
