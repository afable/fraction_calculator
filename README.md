------------------------------------------------------
Fraction_Calculator
------------------------------------------------------
A simple fractional calculator iOS app.

The final project of Programming in Objective-C by Stephen Kochan. I developed and tested the fractional calculator app in Xcode 4 using Xcode's iOS simulator. Unfortunately, I did not have a physical iPhone to test.
